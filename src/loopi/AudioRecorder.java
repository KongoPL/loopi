/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loopi;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import java.io.ByteArrayOutputStream;

/**
 *
 * @author Kongo
 */
public class AudioRecorder implements Runnable
{
	private Thread thread;
	private final String threadName;

	private final AudioFormat audioFormat = new AudioFormat(44100.0f, 16, 2, true, false);
	private ByteArrayOutputStream audioData = new ByteArrayOutputStream();

	protected Action currentAction = Action.Idle;
	
	protected enum Action		
	{
		Idle,
		PlayAudio,
		RecordAudio,
	};

	AudioRecorder(String name)
	{
		this.threadName = name;
	}

	public void start ()
	{
		if (this.thread == null)
		{
			this.thread = new Thread(this, this.threadName);
			this.thread.start();
		}
	}


	public void run()
	{
		while(true)
		{
			this.chooseActionToDo();
		}
	}
	
	
	protected void chooseActionToDo()		
	{
		switch(this.currentAction)
		{
			case Idle:
				return;
				
			case RecordAudio:
				this.recordAudio();
				break;
				
			case PlayAudio:
				this.playAudio();
				break;
		}
	}


	public void startRecording()
	{
		this.currentAction = Action.RecordAudio;
	}


	public void stopRecording()
	{
		this.currentAction = Action.Idle;
	}


	public void playback()
	{
		this.playback(true);
	}


	public void playback(boolean loop)
	{
		this.currentAction = Action.PlayAudio;
	}

	public void stopPlayback()
	{
		this.currentAction = Action.Idle;
	}
	
	
	public void removeAudioData()
	{
		this.audioData.reset();
	}
	

	/**
	 * Following function have to work as blocking function until other thread (main) doesn't call it to stop (by changing state)
	 */
	private void recordAudio()
	{
		try
		{
			DataLine.Info info = new DataLine.Info(TargetDataLine.class, this.audioFormat);
			TargetDataLine microphone = (TargetDataLine) AudioSystem.getLine(info);
			microphone.open(this.audioFormat);

			int CHUNK_SIZE = 1024;
			byte[] data = new byte[microphone.getBufferSize() / 5];
			microphone.start();

			while(this.currentAction == Action.RecordAudio)
			{
				int numBytesRead = microphone.read(data, 0, CHUNK_SIZE);
				
				this.audioData.write(data, 0, numBytesRead); 
			}

			microphone.close();
		} 
		catch(LineUnavailableException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Following function have to work as blocking function until other thread (main) doesn't call it to stop (by changing state)
	 */
	private void playAudio()
	{
		try
		{
			DataLine.Info dataLineInfo = new DataLine.Info(SourceDataLine.class, this.audioFormat);
			SourceDataLine speakers = (SourceDataLine) AudioSystem.getLine(dataLineInfo);
			
			speakers.open(this.audioFormat);
			speakers.start();
			
			int playbackChunkSize = 1024;
			byte[] audioBytes = this.audioData.toByteArray();
			
			for(int i = 0; i < audioBytes.length; i += playbackChunkSize)
			{
				speakers.write(audioBytes, i, playbackChunkSize);
				
				if(this.currentAction == Action.PlayAudio)
				{
					if(i + playbackChunkSize >= audioBytes.length)
					{
						//Next call gonna terminate loop, so we have to reset this loop to play sound again!
						i = -playbackChunkSize;
					}
				}
				else
				{
					break;
				}
			}
						
			speakers.flush();
			speakers.close();
		}
		catch(LineUnavailableException e)
		{
			e.printStackTrace();
		}
	}
}
