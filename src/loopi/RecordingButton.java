/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loopi;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

/**
 *
 * @author Kongo
 */
public class RecordingButton extends Button
{
	protected boolean isPlaying = false;
	protected boolean isRecording = false;
	protected boolean hasAudio = false;
	
	protected AudioRecorder micThread = new AudioRecorder("mic");
	
	RecordingButton _this = this;
	
	RecordingButton()		
	{
		this.initState();
		this.micThread.start();
	}
	
	
	protected void initState()
	{
		this.micThread.stopPlayback();
		this.micThread.stopRecording();
		this.micThread.removeAudioData();
		
		this.isPlaying = false;
		this.isRecording = false;
		this.hasAudio = false;
		
		this.setText("Record audio");
		this.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event)
			{
				_this.startRecording();
			}
		});
	}
	
	
	protected void startRecording()	
	{
		this.isRecording = true;
		this.setText("Stop recording audio...");
		this.micThread.startRecording();
		
		this.setOnAction((ActionEvent event) ->
		{
			_this.stopRecording();
		});
	}
	
	
	protected void stopRecording()
	{
		this.micThread.stopRecording();
		
		this.isRecording = false;
		this.hasAudio = true;
		
		this.setText("Play sound");
		
		this.setOnAction((ActionEvent event) ->
		{
			_this.playSound();
		});
	}
	
	
	protected void playSound()
	{
		this.isPlaying = true;
		this.micThread.playback();
		
		this.setText("Remove sound");
		
		this.setOnAction((ActionEvent event) ->
		{
			_this.initState();
		});
	}
}
